﻿using UnityEngine;

public class lookAtMouse : MonoBehaviour
{
    [SerializeField]
    private float mouseFollowSpeed = 100;
    public static Vector3 targetPoint;
    void FixedUpdate()
    {
        Plane playerPlane = new Plane(Vector3.up, transform.position);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float hitdist = 0.0f;
        if (playerPlane.Raycast(ray, out hitdist))
        {
            targetPoint = ray.GetPoint(hitdist);
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, mouseFollowSpeed * Time.deltaTime);
        }
    }
}
