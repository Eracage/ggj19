﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameManager : MonoBehaviour
{
    public static gameManager gm;

    private void Awake()
    {
        gm = this;
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene().name == "Lose" || SceneManager.GetActiveScene().name == "win")
        {
            if (Input.GetKey(KeyCode.Space))
            {
                MainMenu();
            }
        }
    }

    public void MainMenu()
    {
        SceneManager.LoadSceneAsync("Main Menu");
    }
    public void Lose()
    {
        SceneManager.LoadSceneAsync("Lose");
    }
    public void win()
    {
        SceneManager.LoadSceneAsync("win");
    }
    public void play()
    {
        SceneManager.LoadSceneAsync("GameScene");
    }
    public void story()
    {
        SceneManager.LoadSceneAsync("Story");
    }
    public void quit()
    {
        Application.Quit();
    }
}
