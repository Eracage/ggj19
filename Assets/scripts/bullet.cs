﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class bullet : MonoBehaviour
{
    public float speed;

    private void Start()
    {
        StartCoroutine(Destroyaftertime());
    }
    private void Update()
    {
        transform.position = transform.position + transform.forward * speed * Time.deltaTime;
    }

    IEnumerator Destroyaftertime()
    {
        yield return new WaitForSecondsRealtime(3);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Zombie"))
        {
            other.GetComponent<Zombie>().death(true);
            player.score++;
        }
    }
}
