﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sequence
{
    public GameObject obj;
    public float time;
}

public class Tarina : MonoBehaviour
{
    bool hasLoaded = false;
    public List<Sequence> objects;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float time = 0;
        for (int i = 0; i < objects.Count; i++)
        {
            if (time < Time.timeSinceLevelLoad)
            {
                objects[i].obj.SetActive(true);
            }
            time += objects[i].time;
        }
        if (time < Time.timeSinceLevelLoad && !hasLoaded)
        {
            gameManager.gm.play();
            hasLoaded = true;
        }
    }
}
