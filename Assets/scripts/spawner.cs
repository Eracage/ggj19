﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
    [SerializeField]
    private float spawnRange = 5;
    [SerializeField]
    private float spawnRateMin;
    [SerializeField]
    private float spawnRateMax;
    [SerializeField]
    private float spawnRateMultiplier = 0.95f;
    private float timeTillNextSpawn;
    private float timeTillNextSpawnCounter;
    [SerializeField]
    GameObject spawnPrefab = null;
    private Vector3 spawnPos;
    [SerializeField]
    private float SpawnY = 0;
    [SerializeField]
    private bool ADDS_WAVE = false;
    public static int Wave;        

    void Start()
    {

        setTime();
        StartCoroutine(clock());
    }

    void setTime()
    {
        spawnRateMax = spawnRateMax * spawnRateMultiplier;
        spawnRateMin = spawnRateMin * spawnRateMultiplier;
        timeTillNextSpawnCounter = 0;
        timeTillNextSpawn = Random.Range(spawnRateMin, spawnRateMax);
    }

    void setPos()
    {
        spawnPos.x = Random.Range(gameObject.transform.position.x - spawnRange, gameObject.transform.position.x + spawnRange);
        spawnPos.z = Random.Range(gameObject.transform.position.z - spawnRange, gameObject.transform.position.z + spawnRange);
        spawnPos.y = SpawnY;
    }

    void Spawn(GameObject prefab)
    {
        if (ADDS_WAVE)
        {
            Wave++;
        }
        setPos();
        Instantiate(prefab,spawnPos,Quaternion.identity);
        setTime();
        StartCoroutine(clock());
    }

    IEnumerator clock()
    {
        while (timeTillNextSpawn > timeTillNextSpawnCounter)
        {
            yield return new WaitForSecondsRealtime(1);
            timeTillNextSpawnCounter += 1;
        }
        Spawn(spawnPrefab);
    }
}
