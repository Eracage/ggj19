﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Zombie : MonoBehaviour
{
    private NavMeshAgent agent;
    public int HP = 2;
    [SerializeField]
    private int Dmg = 2;
    [SerializeField]
    private float movementSpeed = 1;

    GameObject House;
    GameObject Player;


    public Animator anim;

    private Rigidbody RB;
    private Vector3 movePosition;
    private bool alive = true;

    private float HouseDist;
    private float PlayerDist;
    private float targetDist;
    public float damageDist = 1;
    private bool canAttack = true;
    public Vector3[] destinations = new Vector3[4];
    private Vector3 destination;
    int destinationNum;

    public void TakesDmg(int dmg)
    {
        HP -= dmg;
        if(HP <= 0)
        {
            death(true);
        }
    }
   
    void Start()
    {
        
        destinationNum = Random.Range(1, 4);
        agent = gameObject.GetComponent<NavMeshAgent>();
        House = GameObject.FindWithTag("House");
        RB = gameObject.GetComponent<Rigidbody>();
        destinations[0] = new Vector3(-8.5f, 0, 2);
        destinations[1] = new Vector3(-4, 0, -2);
        destinations[2] = new Vector3(-8, 0, -9);
        destinations[3] = new Vector3(6.8f, 0,-9);
        destination = destinations[destinationNum];
        agent.SetDestination(destination);

        anim = gameObject.GetComponentInChildren<Animator>();
    }

    void Update()
    {
        if (alive)
        {
          float dist = Vector3.Distance(destination, gameObject.transform.position);
            if (dist < 1)
            {
                if(canAttack)
                {
                    StartCoroutine(DamageHouse());
                    anim.SetInteger("DesiredState", 12);
                }
            }
            else
            {
                anim.SetInteger("DesiredState", 11);
            }
        }
        else
        {
            transform.Rotate(10, 0, 0);
        }
    }

    public void death(bool animation)
    {
        if(!animation)
        {
            alive = false;
            StartCoroutine(DeleteDelay());
        }
        else
        {
            //animation.Play();
            //if(animation.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
            //{
            //  Destroy(gameobject);
            //}
        }
    }

    IEnumerator DamageHouse()
    {
        canAttack = false;
        yield return new WaitForSecondsRealtime(1);
        canAttack = true;
        House.GetComponent<house>().takeDmg(Dmg);
    }

    IEnumerator DeleteDelay()
    {
        yield return new WaitForSecondsRealtime(1);
        Destroy(gameObject);
    }
}