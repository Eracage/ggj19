﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Animations;

[RequireComponent(typeof(CharacterController))]
public class player : MonoBehaviour
{

    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI HP;
    public TextMeshProUGUI Wave;
    public static bool Throws;
    public Animator anim;
    public AnimationClip Walk;
    public AnimationClip Idle;
    public AnimationClip Throw;

    [SerializeField]
    private float movementSpeed= 5;
    private CharacterController cc;
    private float verticalvelocity = 0;
    [SerializeField]
    private Itemsystem IS = null;

    public static int score;
    

    public int health = 20;

    void Start()
    {
        Walk.name = "Walk";
        Idle.name = "Idle";
        Throw.name = "Throw";
        Throws = false;
        anim = gameObject.GetComponentInChildren<Animator>();
        IS = gameObject.GetComponent<Itemsystem>();
       cc = GetComponent<CharacterController>();
    }

    void FixedUpdate()
    {
        Wave.text = ("Wave " + spawner.Wave.ToString());
        scoreText.text = (score.ToString());
        HP.text = (house.hp.ToString()+"/100HP");

        if (spawner.Wave == 10)
        {
            //victory
             gameManager.gm.win();
        }
        if(Input.GetKeyUp(KeyCode.Mouse0))
        {
            Throws = true;
            IS.playerItemsUse();
        }

        float forwardSpeed = Input.GetAxis("Horizontal") * -movementSpeed;
        float sideSpeed = Input.GetAxis("Vertical") * movementSpeed;
        verticalvelocity += Physics.gravity.y * Time.deltaTime;
        Vector3 speed = new Vector3(sideSpeed, verticalvelocity, forwardSpeed);
        speed = transform.rotation * speed;
        cc.Move(speed * Time.deltaTime);

        if(anim.GetCurrentAnimatorClipInfo (0).Length == 0)
        {
            return;
        }
        else
        {
        }

        if(forwardSpeed != 0 || sideSpeed != 0)
        {
            anim.SetInteger("DesiredState", 1);
        }
        else
        {
            anim.SetInteger("DesiredState", 0);
        }

        if(Throws)
        {
            anim.SetInteger("DesiredState", 2);
            Throws = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("item"))
        {
            IS.pickUp(other.gameObject.GetComponent<itemPrefab>().item.itemType, other.gameObject.GetComponent<itemPrefab>().item.ItemCount);
            Destroy(other.gameObject);
        }
    }

    public void TakeDMG(int dmg)
    {
        health -= dmg;
    }
}
