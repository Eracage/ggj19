﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public float speed;
    CapsuleCollider cc;
    public float startTime;

    private void Start()
    {
        StartCoroutine(Destroyaftertime());
        StartCoroutine(AddCollider());

    }
    private void Update()
    {
        transform.position = transform.position + transform.forward * speed * Time.deltaTime;
    }

    IEnumerator Destroyaftertime()
    {
        yield return new WaitForSecondsRealtime(3);
        Destroy(gameObject);
    }
    IEnumerator AddCollider()
    {
        yield return new WaitForSecondsRealtime(startTime);
        gameObject.AddComponent<CapsuleCollider>();
        cc = GetComponent<CapsuleCollider>();
        cc.center.Set(0, 0.5f, 0);
        cc.radius = 1.5f;
        cc.height = 1;
        gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Zombie"))
        {
            Vector3 underStrike = new Vector3(other.transform.position.x, other.transform.position.y - 1, other.transform.position.z);
            other.GetComponent<Zombie>().death(false);
            other.GetComponent<Rigidbody>().AddExplosionForce(1000, transform.position, 5);
            gameObject.GetComponent<Rigidbody>().AddExplosionForce(1000, transform.position, 5);
            player.score++;
        }
    }
}