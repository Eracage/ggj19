﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;   

[RequireComponent(typeof(NavMeshAgent))]
public class SWAN : MonoBehaviour
{
    NavMeshAgent nav;
    float range = 5;
    float waitTime = 5;


    private void Start()
    {
        nav = gameObject.GetComponent<NavMeshAgent>();
        setDestination();
    }

    void setDestination()
    {
        Vector3 destination = new Vector3(Random.Range(transform.position.x - range, transform.position.x + range), 0, Random.Range(transform.position.z - range, transform.position.z + range));
        nav.SetDestination(destination);
        StartCoroutine(clock());
    }

    IEnumerator clock()
    {
        yield return new WaitForSecondsRealtime(waitTime);
        setDestination();
    }
}
