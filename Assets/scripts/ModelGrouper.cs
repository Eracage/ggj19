﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameObjectList
{
    public List<GameObject> gameObjects;
}

public class ModelGrouper : MonoBehaviour
{
    public List<GameObjectList> Groups;
}
