﻿using UnityEngine;

public enum ItemType
{
    coin,
    gum,
    egg,
    joutsen
}

[System.Serializable]
public class Item
{
    public int ItemCount;
    public ItemType itemType;
    public bool Acquired;
    public bool selectable;
    public KeyCode kc;
    public GameObject obj;
    public GameObject sprite;
}
