﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Itemsystem : MonoBehaviour
{
    public Item[] items = new Item[5];
    //public Inventory inventory;
    public Item curItem;

    public Transform spawnPos;

    void Start()
    {
        curItem = items[3];
    }

    void Update()
    {
        foreach (Item i in items)
        {
            if (Input.GetKey(i.kc))
            {
                if(i.selectable)
                {
                    curItem = i;
                }
                else
                {
                    useItem(i);
                }
            }
        }
        UpdateUI();
    }


    public void pickUp(ItemType t, int itemAmount)
    {
        player.Throws = true;
        foreach(Item i in items)
        {
            if(t == i.itemType)
            {
                if(!i.Acquired)
                {

                    i.Acquired = true;
                }
                i.ItemCount += itemAmount;
            }
        }
    }
    
    void useItem(Item i)
    {
        if (i.ItemCount > 0)
        {
            Instantiate(i.obj,spawnPos.position,gameObject.GetComponentInChildren<lookAtMouse>().gameObject.transform.rotation);
            i.ItemCount--;
        }
    }

    public void playerItemsUse()
    {
        useItem(curItem);
    }

    void UpdateUI()
    {
        foreach (Item i in items)
        {
            i.sprite.GetComponentInChildren<TextMeshProUGUI>().text = (i.ItemCount.ToString());
            i.sprite.GetComponent<Image>().color = new Color(255,255,255);
            if (i == curItem)
            {
                i.sprite.GetComponentInChildren<TextMeshProUGUI>().color = new Color(255, 0, 0);
                i.sprite.GetComponent<Image>().color = new Color(255, 0, 0);
                i.sprite.GetComponentInChildren<Image>().color = new Color(255, 0, 0);
            }
            else
            {
                i.sprite.GetComponentInChildren<TextMeshProUGUI>().color = new Color(0, 0, 0);
                i.sprite.GetComponent<Image>().color = new Color(0, 0, 0);
                i.sprite.GetComponentInChildren<Image>().color = new Color(0, 0, 0);
            }
        }
    }

}
